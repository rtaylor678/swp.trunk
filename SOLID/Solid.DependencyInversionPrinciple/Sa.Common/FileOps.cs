﻿using System;
using System.IO;

namespace Sa
{
    public static class FileOps
    {
		/// <summary>
		/// Deletes a file; verifies the file has been deleted.
		/// </summary>
		/// <param name="PathDelete">Path to file to be deleted.</param>
		/// <returns>Boolean</returns>
		/// <remarks>Loops tyring to delete a file.  Necessary because simulation's do not timely close.</remarks>
		public static bool DeleteFile(string path)
		{
			const int Iterations = 0;
			int i = 0;

			while (File.Exists(path) && i <= Iterations)
			{
				GC.WaitForPendingFinalizers();
				GC.Collect();
				GC.WaitForFullGCComplete();

				try
				{
					File.Delete(path);
				}
				catch (IOException exIO)
				{
					Console.WriteLine(exIO);
				}
				catch (UnauthorizedAccessException exUac)
				{
					Console.WriteLine(exUac);
				}
				finally
				{
					i++;
				}
			}

			return (!File.Exists(path));
		}

		public static bool Copyfile(string pathSource, string pathTarget, bool overwrite = true)
		{
			if (File.Exists(pathSource) &&  (pathSource != pathTarget))
			{
				File.Copy(pathSource, pathTarget, overwrite);
			}

			return File.Exists(pathTarget);
		}

		public static void CopyDirectory(string sourceDirectoryPath, string targetDirectoryPath, bool overwrite, bool recursive)
		{
			// Get the subdirectories for the specified directory.
			DirectoryInfo dir = new DirectoryInfo(sourceDirectoryPath);
			DirectoryInfo[] dirs = dir.GetDirectories();

			if (!dir.Exists)
			{
				throw new DirectoryNotFoundException(
					"Source directory does not exist or could not be found: "
					+ sourceDirectoryPath);
			}

			// If the destination directory doesn't exist, create it. 
			if (!Directory.Exists(targetDirectoryPath))
			{
				Directory.CreateDirectory(targetDirectoryPath);
			}

			// Get the files in the directory and copy them to the new location.
			FileInfo[] files = dir.GetFiles();
			foreach (FileInfo file in files)
			{
				string temppath = Path.Combine(targetDirectoryPath, file.Name);
				file.CopyTo(temppath, overwrite);
			}

			// If copying subdirectories, copy them and their contents to new location. 
			if (recursive)
			{
				foreach (DirectoryInfo subdir in dirs)
				{
					string temppath = Path.Combine(targetDirectoryPath, subdir.Name);
					CopyDirectory(subdir.FullName, temppath, overwrite, recursive);
				}
			}
		}
    }
}
